import "./App.css";

import { BooksSearch } from "./features/booksSearch/BooksSearch";
import Container from "react-bootstrap/Container";
import React from "react";

function App() {
  return (
    <Container fluid>
      <div className="App">
        <BooksSearch />
      </div>
    </Container>
  );
}

export default App;
