import React, { useEffect, useRef, useState } from "react";

import { Card } from "react-bootstrap";

const MAX_POSSIBLE_HEIGHT = 500;

const ExpendableText = ({ maxHeight, text }) => {
  const ref = useRef();
  const [shouldShowExpand, setShouldShowExpand] = useState(false);
  const [expanded, setExpanded] = useState(true);

  useEffect(() => {
    if (ref.current.scrollHeight > maxHeight) {
      setShouldShowExpand(true);
      setExpanded(false);
    }
  }, [maxHeight]);

  return (
    <Card.Text ref={ref}>
      <div
        style={{
          overflow: "hidden",
          transition: "max-height 0.2s ease",
          maxHeight: expanded ? MAX_POSSIBLE_HEIGHT : maxHeight,
        }}
      >
        {text}
      </div>
      {shouldShowExpand && (
        <button
          style={{
            border: "none",
            marginTop: "10px",
            fontSize: "18px",
            color: "blue",
          }}
          onClick={() => setExpanded(!expanded)}
        >
          Read More
        </button>
      )}
    </Card.Text>
  );
};

export default ExpendableText;
