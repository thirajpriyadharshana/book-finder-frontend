export const SnakeToUpperCaseEachWord = (text) => {
  const words = text.split("_");
  for (let i = 0; i < words.length; i++) {
    words[i] = words[i][0].toUpperCase() + words[i].substr(1);
  }
  return words.join(" ");
};

export const serializeQueryParams = (obj) => {
  let str = [];
  let prefix = "?";
  for (let [param, value] of Object.entries(obj)) {
    console.log("param", param);
    console.log("value", value);

    if (value) {
      str.push(`${encodeURIComponent(param)}=${encodeURIComponent(value)}`);
    }
  }

  return prefix + str.join("&");
};
