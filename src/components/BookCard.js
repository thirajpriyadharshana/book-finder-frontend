import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import ExpendableText from "./ExpandableCardText";
import React from "react";
import Row from "react-bootstrap/Row";

export function BookCard({ books }) {
  const renderCard = () => {
    return books.map((book) => {
      return (
        <Col sm={3} key={book.id}>
          <Card style={{ margin: "5px" }}>
            <Card.Body>
              <Card.Title>{book.title}</Card.Title>
              <Card.Text>Author: {book.author}</Card.Text>

              <Card.Text>Year: {book.published_year}</Card.Text>

              <ExpendableText maxHeight={50} text={book.description} />
            </Card.Body>
          </Card>
        </Col>
      );
    });
  };

  return <Row>{renderCard()}</Row>;
}
