import React, { useState } from "react";

import Pagination from "react-bootstrap/Pagination";

export function Paginator({ totalPages, currentPage, handleClick }) {
  const renderItems = () => {
    let items = [];
    for (let number = 1; number <= totalPages; number++) {
      items.push(
        <Pagination.Item
          key={number}
          active={number === currentPage}
          onClick={() => handleClick(number)}
        >
          {number}
        </Pagination.Item>
      );
    }

    return items;
  };

  return (
    <div
      style={{ display: "flex", justifyContent: "center", marginTop: "10px" }}
    >
      <Pagination>{renderItems()}</Pagination>
    </div>
  );
}
