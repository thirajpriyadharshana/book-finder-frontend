import booksReducer from "../features/booksSearch/bookSearchSlice";
import { configureStore } from "@reduxjs/toolkit";

export const store = configureStore({
  reducer: {
    books: booksReducer,
  },
});
