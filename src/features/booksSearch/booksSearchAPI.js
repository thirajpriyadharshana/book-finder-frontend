import { serializeQueryParams } from "../../helpers/util";

const API_BASE_URL = "http://localhost:8100/api/v1/";

// Make API call to fetch books
export function fetchBooks(queryObj) {
  const queryParams = serializeQueryParams(queryObj);

  return fetch(API_BASE_URL + "search/books" + queryParams)
    .then((response) => response.json())
    .then((parsedData) => parsedData)
    .catch(() => console.log("Book fetching failed"));
}

// Make API call to fetch tags
export function fetchSearchTags() {
  return fetch(API_BASE_URL + "search/tags")
    .then((response) => response.json())
    .then((parsedData) => parsedData)
    .catch(() => console.log("Search Tags fetching failed"));
}
