import React, { useEffect, useState } from "react";
import {
  fetchBooksAsync,
  fetchSearchTagsAsync,
  selectBooks,
  selectPagination,
  selectTags,
} from "./bookSearchSlice";
import { useDispatch, useSelector } from "react-redux";

import { BookCard } from "../../components/BookCard";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import { Paginator } from "../../components/Paginator";
import { SnakeToUpperCaseEachWord } from "../../helpers/util";
import bookStyles from "./BookSearch.module.css";

export function BooksSearch() {
  const books = useSelector(selectBooks);
  const tagsList = useSelector(selectTags);
  const pagingData = useSelector(selectPagination);
  const dispatch = useDispatch();
  const [searchTerm, setSearchTerm] = useState("");
  const [searchTag, setSearchTag] = useState("");
  const [currentPage, setCurrentPage] = useState(1);

  const loadTags = () => {
    dispatch(fetchSearchTagsAsync());
  };

  const fetchBooks = () => {
    if (!searchTerm && searchTag) {
      alert("Please enter your search query");
      return;
    }

    dispatch(fetchBooksAsync({ q: searchTerm, tag: searchTag }));
    setCurrentPage(1);
  };

  const searchByTag = (e) => {
    setSearchTag(() => e.target.value);

    if (searchTerm) {
      fetchBooks();
    }
  };

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
    dispatch(
      fetchBooksAsync({ q: searchTerm, tag: searchTag, page: pageNumber })
    );
  };

  useEffect(() => {
    loadTags();
    dispatch(fetchBooksAsync({ q: searchTerm, tag: searchTag }));
  }, []);

  const renderTagList = () => {
    return (
      <Form.Select
        className={bookStyles.textbox}
        aria-label="Select a tag"
        style={{ borderRadius: 0 }}
        onChange={(e) => searchByTag(e)}
      >
        <option value={""}>Select a tag</option>
        {tagsList.map((tag) => {
          return (
            <option key={tag} value={tag}>
              {SnakeToUpperCaseEachWord(tag)}
            </option>
          );
        })}
      </Form.Select>
    );
  };

  return (
    <div>
      <div className={bookStyles.headerRow}>
        <Col>{renderTagList()}</Col>

        <Col>
          <input
            className={bookStyles.textbox}
            aria-label="Search your favourite books"
            placeholder="Search books here"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
        </Col>

        <Col>
          <button
            className={bookStyles.asyncButton}
            onClick={() => fetchBooks()}
          >
            Search
          </button>
        </Col>
      </div>

      <BookCard books={books} />

      <Paginator
        style={{
          display: "flex",
          marginTop: "20px",
          justifyContent: "center",
          justifyItems: "center",
        }}
        currentPage={currentPage}
        totalPages={pagingData?.last_page}
        handleClick={(pageNumber) => paginate(pageNumber)}
      />
    </div>
  );
}
