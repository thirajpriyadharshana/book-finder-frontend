# Book Search - Frontend with React

Basic react (v18.2) application for search books via api. 

## Initial setup and run locally

- Clone the repository

- Navigate to root directory

- Run `npm install`

- Run `npm start`


### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


### Setup and run backend api first

Please clone and run backend api first. 

### Assumption and Thoughts

- Only built basic UI components since it's not a assignment criteria
- Tag Search: This is not only a frontend search. It's a backend search. I thought here to maintain the paginated result to avoid number of items displayed in the page.
